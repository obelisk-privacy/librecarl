exports.init = (bot) => {
	const logs = '632600479033196628';

	// Log member joins
	bot.on('guildMemberAdd', m => {
		bot.channels.fetch(logs).then(c => {
			let embed = {
				color: 0x2cddae,
				title: `Member joined`,
				description: `${m}\ncreated ${m.user.createdAt}`,
				footer: {
					text: `ID: ${m.id}`
				},
				timestamp: new Date()
			};
	
			c.send({ embed: embed });
		});
	});

	// Log server changes
	bot.on('guildUpdate', (oldGuild, newGuild) => {
		// Log server name changes
		if(oldGuild.name != newGuild.name) {
			bot.channels.fetch(logs).then(c => {
				let embed = {
					color: 0xffac48,
					title: `Server name change`,
					description: `**Before:** ${oldGuild.name}\n**+After:** ${newGuild.name}`,
					footer: {
						text: `ID: ${newGuild.id}`
					},
					timestamp: new Date()
				};
		
				c.send({ embed: embed });
			});
		}
	})

	// Log changed member
	bot.on('guildMemberUpdate', (oldMember, newMember) => {
		// Log nickname changes
		if(oldMember.nickname != newMember.nickname) {
			bot.channels.fetch(logs).then(c => {
				let embed = {
					color: 0xffac48,
					title: `Nickname change`,
					author: {
						name: newMember.user.tag,
						icon_url: newMember.user.avatarURL()
					},
					description: `**Before:** ${oldMember.nickname}\n**+After:** ${newMember.nickname}`,
					footer: {
						text: `ID: ${newMember.id}`
					},
					timestamp: new Date()
				};
		
				c.send({ embed: embed });
			});
		// Log role changes
		} else if (oldMember.roles != newMember.roles) {
			Array.prototype.diff = function(a) { return this.filter(function(i) {return a.indexOf(i) < 0;}) };
			
			const oldRoles = oldMember.roles.cache.array();
			const newRoles = newMember.roles.cache.array()
			
			const oldDiff = oldRoles.diff(newRoles);
			const newDiff = newRoles.diff(oldRoles);

			// Log role added
			if(newDiff && newDiff.length > 0) {
				bot.channels.fetch(logs).then(c => {
					let embed = {
						color: 0xffac48,
						title: `Role added`,
						author: {
							name: newMember.user.tag,
							icon_url: newMember.user.avatarURL()
						},
						description: newDiff[0].toString(),
						footer: {
							text: `ID: ${newMember.id}`
						},
						timestamp: new Date()
					};
			
					c.send({ embed: embed });
				});
			// Log role removed
			} else if (oldDiff && oldDiff > 0) {
				bot.channels.fetch(logs).then(c => {
					let embed = {
						color: 0xffac48,
						title: `Role removed`,
						author: {
							name: newMember.user.tag,
							icon_url: newMember.user.avatarURL()
						},
						description: oldDiff[0].toString(),
						footer: {
							text: `ID: ${newMember.id}`
						},
						timestamp: new Date()
					};
			
					c.send({ embed: embed });
				});
			}
		}
	});

	// Log edited messages
	bot.on('messageUpdate', (oldMessage, newMessage) => {
		if(!newMessage.author || newMessage.author.bot) return;

		if(oldMessage.content != newMessage.content) {
			bot.channels.fetch(logs).then(c => {
				let embed = {
					color: 0xffac48,
					title: `Message edited in #${newMessage.channel.name}`,
					author: {
						name: newMessage.author.tag,
						icon_url: newMessage.author.avatarURL()
					},
					description: `**Before:** ${oldMessage.content}\n**+After:** ${newMessage.content}`,
					footer: {
						text: `ID: ${newMessage.id}`
					},
					timestamp: new Date()
				};
		
				c.send({ embed: embed });
			});
		}
	});

	// Log deleted messages
	bot.on('messageDelete', (message) => {
		if(!message.author || message.author.bot) return;

		bot.channels.fetch(logs).then(c => {
			let embed = {
				color: 0xe55f57,
				title: `Message deleted in #${message.channel.name}`,
				author: {
					name: message.author.tag,
					icon_url: message.author.avatarURL()
				},
				description: message.content,
				footer: {
					text: `ID: ${message.author.id}`
				},
				timestamp: new Date()
			};

			c.send({ embed: embed });
		});
	});
}