// Retrieve environment variables
require('dotenv').config();

// Keep the bot online
const express = require("express");
const app = express();

app.listen(() => console.log("Server started"));

app.use('/', (req, res) => {
  res.send(new Date());
});

// Setup Keyv
const Keyv = require('keyv');
const keyv = new Keyv();

keyv.on('error', err => console.error('Keyv connection error:', err));

// Initialize the bot
const Discord = require('discord.js');
const bot = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });

const isAdmin = (member) => member.roles.cache.some(role => role.name === 'Moderator');

// Storage
const pf = '?'; // Define prefix
const suggestions = '632602567083294721';

bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}!`);
});

// Commands
bot.on('message', message => {
	if(message.content.startsWith(pf) && !message.author.bot) {
		const reply = (msg) => message.channel.send(msg);
		const args = message.content.slice(pf.length).split(/\s+/);

		switch (args[0]) {
			// Fun

			case 'hi':
				reply('Hi!');
				break;

			case 'borat':
				reply('https://tenor.com/wpmn.gif');
				break;

			// Utilities

			case 'suggest':
				bot.channels.fetch(suggestions).then(c => {
					let embed = {
						color: 0xffac48,
						title: `Suggestion`,
						description: message.content.replace(`${pf}suggest `, ''),
						author: {
							name: message.author.tag,
							icon_url: message.author.avatarURL()
						}
					};
			
					c.send({ embed: embed }).then(m => {
						m.react('⬆️').then(() => m.react('⬇️'));
					});
				});

				message.delete();
				break;

			// Moderation

			case 'ban':
			case 'yeet':
				if(args.length != 2) { return reply(`😩 ${pf}ban takes one parameter.`) }
				message.mentions.members.first().ban();
				reply('📤 Member banned.');
				break;

			case 'unban':
			case 'yayeet':
				message.channel.guild.members.unban()
				break;

			case 'prune':
				if(!isAdmin(message.member)) { return reply('🙈 Invalid permisisons.') }
				if(args.length != 2) { return reply(`😩 ${pf}prune takes one parameter.`) }
				if(isNaN(parts[1])) { return reply(`🔢 First parameter should be an integer.`) }
				message.channel.bulkDelete(parts[1], true);
				reply(`🗑 ${args[1].toString()} messages deleted.`).then(m => setTimeout(() => m.delete(), 2500));
				break;

			case 'sm':
			case 'slowmode':
				if(!isAdmin(message.member)) { return reply('🙈 Invalid permisisons.') }
				if(args.length == 1) { message.channel.setRateLimitPerUser(0); return reply('📴 Slowmode disabled.') }
				if(isNaN(args[1])) return reply(`😩 Slowmode needs a number!`);
				message.channel.setRateLimitPerUser(parseInt(parts[1]));
				reply(`✅ Set channel slowmode to 1 message per ${parts[1]} second(s).`);
				break;

			// Miscellaneous

			case 'ping':
				reply(`🏓 **Pong!** Took ${Date.now() - message.createdTimestamp}ms`);
				break;

			default:
				break;
		}
	}
});

require('./logging').init(bot);

bot.login(process.env.token);